#+title: [Notes] Network utilities
#+date: <2012-03-24>
#+filetags: linux network ifconfig ip route nmcli ssh
#+setupfile: ../org-templates/post.org

Few network configuration which can be performed using CLI, this post covers
examples with syntax.

(Updated on May 25, 2020)

/Note/: =root= or =sudo= access is required to run few commands. =ifconfig= has
been deprecated in favor of =ip=.

** Assign IP address
   :PROPERTIES:
   :ID:       ac9cf0b9-5b67-4c37-889f-5c727e5e1cae
   :END:

   Syntax
   #+BEGIN_SRC bash -n
     ifconfig <INTERFACE> <IP-ADDRESS> netmask <NETMASK>
     # or
     ip addr add <IP-ADDRESS/CIDR> dev <INTERFACE>
   #+END_SRC

   Example
   #+BEGIN_SRC bash -n
     ifconfig eth0 192.168.1.11 netmask 255.255.255.0
     # or
     ip addr add 192.168.1.11/24 dev eth0
   #+END_SRC

** Add gateway/route
   :PROPERTIES:
   :ID:       b08bf55d-95cf-41ec-b259-da3077fa438d
   :END:

   Syntax
   #+BEGIN_SRC bash -n
     route add default gw <GATEWAY-IP> <INTERFACE>
     # or
     ip route add default via <GATEWAY-IP> dev <INTERFACE>
   #+END_SRC

   Example
   #+BEGIN_SRC bash -n
     route add default gw 192.168.1.1 eth0
     # or
     ip route add default via 192.168.1.1 dev eth0
   #+END_SRC

** Temporary spoof MAC address
   :PROPERTIES:
   :ID:       aff8abd3-39ef-49a3-981d-665fb2df8c18
   :END:

   Syntax
   #+BEGIN_SRC bash -n
     ip link set down dev DEVICE_NAME
     ip link set dev DEVICE_NAME address AA:BB:CC:DD:EE:FF
     ip link set up dev DEVICE_NAME
   #+END_SRC

   Example
   #+BEGIN_SRC bash -n
     ip link set down dev enp0s21
     ip link set dev enp0s21 address AA:BB:CC:DD:EE:FF
     ip link set up dev enp0s21
   #+END_SRC

** Set DNS address
   :PROPERTIES:
   :ID:       fb2f8865-7f13-45cf-ab06-73e6a67f3dc8
   :END:

   Optionally DNS can be entered in the file =/etc/resolv.conf= in
   following format

   #+BEGIN_SRC bash -n
     # /etc/resolv.conf
     nameserver 8.8.8.8
     nameserver 8.8.4.4
   #+END_SRC

   All the above changes will be temporary(unless you reboot the system)

** Additional scenario
   :PROPERTIES:
   :ID:       6cd6bbb6-3d2c-4b4b-b2ea-99c1c50543e9
   :END:

   - You want 10.10.10.x address space to bypass default gateway of
     the network. You can reach network range of 10.10.10.0/24 via
     192.168.1.11 on device =eth0=

     #+BEGIN_SRC bash
       ip route add 10.10.10.0/24 via 192.168.1.11 dev eth0
     #+END_SRC


   - Make routes persistent(on Fedora/RHEL)

     Add following entry into the file
     =/etc/sysconfig/network-scripts/route-DEVICE_NAME=

     #+BEGIN_SRC bash
       10.10.10.0/24 via 192.168.1.11 dev DEVICE_NAME
     #+END_SRC

** CLI to control NetworkManager
    :PROPERTIES:
    :ID:       1336ac1b-1b71-49ab-ac18-144756a85de3
    :END:

*** Check overall status
    :PROPERTIES:
    :ID:       88fa49dc-993c-4ed1-ac19-1307fc32d16c
    :END:
    #+BEGIN_SRC sh
      nmcli general status
    #+END_SRC

*** Show all connections
    :PROPERTIES:
    :ID:       7c71ba0f-dedf-4dc6-80eb-d9d893ef91b5
    :END:
    #+BEGIN_SRC sh
      nmcli connection
    #+END_SRC

*** Show all devices
    #+BEGIN_SRC bash -n
      nmcli device

      # Sample output
      DEVICE      TYPE      STATE        CONNECTION
      enp0s25     ethernet  connected    enp0s25
      virbr0      bridge    connected    virbr0
      wlp3s0      wifi      unavailable  --
      lo          loopback  unmanaged    --
      virbr0-nic  tun       unmanaged    --
    #+END_SRC

*** Show details for specific connection
    :PROPERTIES:
    :ID:       99c2ef59-f4ee-4eaa-b739-7b973413fcb0
    :END:

    Syntax
    #+BEGIN_SRC sh
      nmcli connection show <GENERAL.NAME>
    #+END_SRC

    Example
    #+BEGIN_SRC sh
      nmcli connection show my-dsl-conn
    #+END_SRC

*** Connect using connection name
    :PROPERTIES:
    :ID:       96d5a755-7a62-45ba-8d93-291c083054cb
    :END:

    Syntax
    #+BEGIN_SRC sh
      nmcli connection up <GENERAL.NAME>
    #+END_SRC

    Example
    #+BEGIN_SRC sh
      nmcli connection up my-dsl-conn
    #+END_SRC

*** Show status of the WIFI
    #+BEGIN_SRC bash -n
      nmcli radio

      # Sample output
      WIFI-HW  WIFI      WWAN-HW  WWAN
      enabled  disabled  enabled  disabled
    #+END_SRC

*** Enable WIFI
    #+BEGIN_SRC bash
      nmcli radio wifi on
    #+END_SRC

*** Networking Status/Enable/Disable

    Check Network status
    #+BEGIN_SRC bash -n
      nmcli networking

      # Sample output
      enabled
    #+END_SRC

    Force NetworkManager to re-checks the connectivity
    #+BEGIN_SRC bash -n
      nmcli networking connectivity check

      # Sample output
      full
    #+END_SRC

    Disable networking
    #+BEGIN_SRC bash
      nmcli networking off
    #+END_SRC

    Enable networking
    #+BEGIN_SRC bash
      nmcli networking on
    #+END_SRC

** SSH: Secure shell
*** SOCKS proxy
    #+BEGIN_SRC shell
      ssh -N -D 1080 user@server
    #+END_SRC

    /where/

    *-N*: Do not execute remote commands

    *-D*: =[bind address:]port= (port in the above example)

*** Local port forwarding
    Forward all the requests from local port to remote port via /remote-server/.

    Example:
    #+BEGIN_SRC shell
      ssh -L 8000:blocked-domain.com:80 user@remote-server
    #+END_SRC

    In the above example the website /blocked-domain.com/ is not accessible from
    local machine(may be because it is blocked) but it can be accessed from
    /remote-server/. We create an SSH tunnel to /remote-server/ and forward all
    the request from local port 8000 to port 80 of /blocked-domain.com/ via
    /remote-server/. Once the connection is established, the
    /blocked-domain.com/ can be accessed from local machine on port
    8000(localhost:8000). The domain /blocked-domain.com/ hence assumes that all
    the requests are coming from /remote-server/.

    Syntax:
    #+BEGIN_SRC shell
      ssh -L <LOCAL_PORT>:<REMOTE_HOST>:<REMOTE_PORT> user@remote-server
    #+END_SRC

    Another example of forwarding local port is found in the manpage of =ssh=:

    When encrypting communication between an IRC client and server, even though
    the IRC server does not directly support encrypted communications. This
    works as follows: the user connects to the remote host using ssh,
    specifying a port to be used to forward connections to the remote server.
    After that it is possible to start the service which is to be encrypted on
    the client machine, connecting to the same local port, and ssh will encrypt
    and forward the connection.

    The following example tunnels an IRC session from client machine "127.0.0.1"
    (localhost) to remote server "server.example.com":
    #+BEGIN_SRC shell
      ssh -f -L 1234:localhost:6667 server.example.com sleep 10
      irc -c '#users' -p 1234 pinky 127.0.0.1
    #+END_SRC

    This tunnels a connection to IRC server server.example.com", joining
    channel "#users", nickname "pinky", using port 1234. It doesn't matter which
    port is used, as long as it's greater than 1023 (remember, only root can
    open sockets on privileged ports) and doesn't conflict with any ports
    already in use. The connection is forwarded to port 6667 on the remote
    server, since that's the standard port for IRC services.

    The -f option backgrounds ssh and the remote command =sleep 10= is specified
    to allow an amount of time (10 seconds, in the example) to start the service
    which is to be tunnelled. If no connections are made within the time
    specified, ssh will exit.

*** Remote port forwarding
    Forward all requests from the remote port to local port.

    Example:
    #+BEGIN_SRC shell
      ssh -R 8000:localhost:3000 user@remote-server.com
    #+END_SRC

    In the above example assume that we are developing a website and we test it
    locally on port 3000(localhost:3000), but if we want to showcase or
    demonstrate the website to the public, we create a remote tunnel to
    /remote-server.com/ and forward all traffic from /remote-server.com:8000/ to
    /localhost:3000/. Any one accessing /remote-server.com:8000/ will be able to
    access the website.

    Syntax:
    #+BEGIN_SRC shell
      ssh -R <REMOTE_PORT>:localhost:<LOCAL_PORT> user@remote-server.com
    #+END_SRC


#+INCLUDE: "../disquss.inc"

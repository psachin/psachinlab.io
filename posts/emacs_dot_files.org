#+title: GNU Emacs: Show/hide dot files
#+date: <2022-11-10>
#+filetags: emacs
#+setupfile: ../org-templates/post.org

#+HTML: <div class="video-container"><iframe class="video-iframe" width="560" height="315" src="https://www.youtube.com/embed/EuRaCDG671g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
*GNU Emacs: Show/hide dot files*

#+INCLUDE: "../disquss.inc"
